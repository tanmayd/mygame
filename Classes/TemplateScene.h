#ifndef __TEMPLATE_SCENE_H__
#define __TEMPLATE_SCENE_H__

#include "cocos2d.h"

class TemplateScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(TemplateScene);
};

#endif // __TEMPLATE_SCENE_H__
