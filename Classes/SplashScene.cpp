#include "SplashScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* SplashScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = SplashScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

//Test Comment by TD

// on "init" you need to initialize your instance
bool SplashScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    bg = Sprite::create("Splash_Screen_BG.png");
    bg->setPosition(Point((visibleSize.width/2) + origin.x,(visibleSize.height/2) + origin.y));
    bg->setScale(visibleSize.width / bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);

    logoPran = Sprite::create("Splash_Pran_Logo.png");
    //logoPran->setPosition(Point((visibleSize.width/2) + origin.x,(visibleSize.height/2)));
    //logoPran->setAnchorPoint(Vec2(0.0, 0.0));
    logoPran->setPosition(Point((visibleSize.width/2) + origin.x,220));
    //logoPran->setScale(visibleSize.width / logoPran->getContentSize().width, visibleSize.height / logoPran->getContentSize().height);
    logoHW = Sprite::create("HW_Logo.png");
    logoHW->setPosition(Point((visibleSize.width/2) + origin.x,50));
    this->addChild(bg);
    this->addChild(logoPran);
    this->addChild(logoHW);

    //auto action = MoveBy::create(4, Point(50, 150));

    //mySprite->runAction(action);
    
    return true;
}